#+TITLE: The One-More-ISIS Problem
#+BLOG: martinralbrecht
#+POSTID: 2053
#+DATE: [2022-06-01 Wed 15:10]
#+OPTIONS: toc:nil num:nil todo:nil pri:nil tags:nil ^:nil
#+CATEGORY: cryptography
#+TAGS: lattice-based-cryptography, lattices, cryptanalysis, 
#+DESCRIPTION:

In “[[https://eprint.iacr.org/2021/1565][Practical, Round-Optimal Lattice-Based Blind Signatures]]” by Shweta Agrawal, Elena Kirshanova,
Damien Stehle and Anshu Yadav, the authors introduce a new candidate hard lattice problem. They introduce this problem to build blind signatures but in this blog post, I’ll ignore the application and only talk about the cryptanalytic target: *One-more-ISIS*.

#+html:<!--more-->

#+begin_quote
A challenger selects a matrix \(\mathbf{A} \in \mathbb{Z}_{q}^{n \times m}\) and sends it to the adversary. The adversary can perform two types of queries:
1. *Syndrome queries* The adversary can request a challenge vector which the challenger selects at random, i.e. \(\mathbf{t} \gets_{\$} \mathbb{Z}_{q}^{n}\), adds to some set \(\mathcal{S}\) and returns to the adversary.
2. *Preimage queries.* The adversary submits /any/ vector \(\mathbf{t}' \in \mathbb{Z}_{q}^{n}\). The challenger will return a short vector \(\mathbf{y}' \gets_{\$} D_{\mathbb{Z}^m,\sigma}\) such that \(\mathbf{A}\cdot \mathbf{y}' \equiv \mathbf{t}' \bmod q\). Denote \(\ell\) for the number of preimage queries.
In the end the adversary is asked to output \(\ell+1\) pairs \(\{(\mathbf{y}_i,\mathbf{t}_i)\}_{0 \leq i < \ell+1}\) satisfying:
- \(\mathbf{A}\cdot \mathbf{y}_{i} \equiv \mathbf{t}_{i} \bmod q\),
- \(\|\mathbf{y}_{i}\| \leq \beta\) and
- \(\mathbf{t}_{i} \in \mathcal{S}\).
#+end_quote

The hardness of the problem depends on the parameters, critically \(\beta\) and \(\ell\). To see this consider the following two attacks, given in the above mentioned paper.

*Combinatorial Attack.* The adversary requests \(n \cdot q\) preimages for all \(\{a \cdot \mathbf{e}_{i} | a \in \mathbb{Z}_{q}, i \in \mathbb{Z}_{n}\}\), here \(\mathbf{e}_{i}\) is the \(i\)-th unit vector. Then, adding up \(n\) such preimages allows to construct any image. Since the norm of the preimages returned by the challenger is \(\sqrt{m} \cdot \sigma\), this allows to solve the One-more-ISIS problem when \(\sqrt{n\cdot m} \cdot \sigma \leq \beta\). Of course, smaller and larger sets of preimages are possible, increasing and decreasing the output norm respectively.

*Lattice Attack.* The adversary requests \(> m\) preimages of zero and uses that to produce a short basis \(\mathbf{B}\) for the kernel of \(\mathbf{A}\), i.e. \(\mathbf{A}\cdot\mathbf{B} \equiv \mathbf{0} \bmod q\). This constitutes a trapdoor for \(\mathbf{A}\) and thus permits to return short preimages for any target. The key point here is that this trapdoor is of degraded quality relative to the trapdoor used by the challenger. The key computational challenge then is to fix-up or improve this degraded trapdoor in order to be able to sample sufficiently short vectors.

I'd say that last computational problem is of more general interest. That is, given some polynomial number of short vectors in a lattice, how hard is it to produce a slightly shorter basis for this lattice? Okay, technically, there might be a way of sampling short preimages without finding such a high-quality basis, but finding such a high-quality basis would certainly solve the problem.

Finally, it's worth noting that this problem is not only relevant for building blind signatures. It would also arise in some side-channel attacks on [[https://eprint.iacr.org/2007/432][GPV-style]] signature schemes such as [[https://falcon-sign.info/][Falcon]]. That is, in these signature schemes, signing the same \(H(m)\) twice would produce a preimage of zero, i.e. \(H(m) \equiv \mathbf{A} \cdot \mathbf{u}_{0} \equiv \mathbf{A} \cdot \mathbf{u}_{1}\) implies \(0 \equiv \mathbf{A} \cdot (\mathbf{u}_{0} - \mathbf{u}_{1})\). Sampling many such preimages of zero would constitute a trapdoor as discussed above. Falcon defends against this by signing \(H(m,r)\) for some fresh random \(r\). Now, in a setting where only poor randomness is available (think [[https://github.com/malb/bdd-predicate/][attacks on Schnorr-like signatures with poor randomness]]) this might collapse down to \(H(m)\) again. Studying lattice attacks on One-more-ISIS would help us to understand how devastating this would be.

# Local Variables:
# eval: (typo-mode -1)
# eval: (org-cdlatex-mode 1)
# End:
