#+TITLE: Senior Research Scientist in Post-Quantum Cryptography at SandboxAQ
#+BLOG: martinralbrecht
#+POSTID: 2136
#+DATE: [2024-02-02 Fri 11:03]
#+OPTIONS: toc:nil num:nil todo:nil pri:nil tags:nil ^:nil
#+CATEGORY: cryptography
#+TAGS: job, sandboxaq, post-quantum-cryptography, 
#+DESCRIPTION:

The post-quantum research team at [[https://www.sandboxaq.com/][SandboxAQ]], i.e. the team I’m in, is [[https://www.sandboxaq.com/careers-list?gh_jid=5072034004][looking to hire]] a full-time researcher to join our team. Currently, the PQC team consists of:

- [[https://dblp.org/pid/50/2767.html][Anand Kumar Narayanan]]
- [[https://dblp.org/pid/27/1744.html][Andreas Hülsing]]
- [[https://dblp.uni-trier.de/pid/71/4606.html][Carlos Aguilar Melchor]]
- [[https://dblp.uni-trier.de/pid/163/8680.html][James Howe]]
- [[https://dblp.uni-trier.de/pid/92/7397.html][Martin Albrecht]]
- [[https://dblp.uni-trier.de/pid/167/3021.html][Nina Bindel]]

and you can get a sense what we’ve been up to by checking out our respective DBLP pages linked above and the SandboxAQ [[https://pub.sandboxaq.com/][publications]] page. Here are some details about the role:

#+begin_quote
The SandboxAQ team is looking for a Research Scientist to help functionalize the next generation of cryptographic systems. A successful candidate will be comfortable with research in post-quantum cryptography. We are open to strong candidates that reinforce existing expertise of the team as well as candidates extending our expertise. They will be part of a team of diverse cryptographers and engineers, where they will play a key role in efficient and effective enablement of the technologies being developed. They can learn more about what we’ve been doing so far by checking out our publications page or the individual DBPL pages of our permanent researchers.

_Core Responsibilities_

- Research and design of new post-quantum cryptography primitives and protocols
- Engage in team collaborations to meet ambitious product and engineering goals
- Present research discoveries and developments including updates and results clearly and efficiently both internally and externally, verbally and in writing

_Minimum Qualifications_

- PhD in Mathematics or Computer Science or equivalent practical experience 
- Strong background in post-quantum cryptography with a proven publication record at flagship conferences
- Deep understanding of cryptographic primitives and protocols
- Capacity to work both as an individual contributor and on collaborative projects with strong teamwork skills

_Preferred Qualifications_

- Experience in C, C++, Rust or Go, or equivalent skills to implement and validate innovative cryptographic constructions and/or protocols
- Experience with the real-world aspects of cryptography
- Experience contributing to open source projects and standardization bodies
- Curiosity in a variety of domains of cryptography, security, privacy, or engineering
#+end_quote
