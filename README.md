This repository holds my [blog](https://martinralbrecht.wordpress.com) posts.

    for file in *.org; do
        date=$(grep "#+DATE" $file | sed 's/#+DATE: \[//' | sed 's/]//' | awk '{print $1, $3}')
        if [ "$file" != "README.org" ]; then
            touch -d "$date" $file
        fi
    done    

